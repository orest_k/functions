const getSum = (str1, str2) => {
  if(typeof str1 !== 'string' || typeof str2 !== 'string') {
    return false;
  }
  for(let str_num of str1.split("")) {
    if(isNaN(parseInt(str_num))) {
      return false;
    }
  }
  for(let str_num of str2.split("")) {
    if(isNaN(parseInt(str_num))) {
      return false;
    }
  }
  if(str1.length === 0) {
    if(str2.length === 0) {
      return '0';
    }
    return str2;
  }
  else if(str2.length === 0) {
    if(str1.length === 0) {
      return '0';
    }
    return str1;
  }
  let arr_result = [];
  let ind = 0;
  str1 = str1.split("").reverse().join("");
  str2 = str2.split("").reverse().join("");
  if(str1.length >= str2.length) {
    while(ind < str1.length) {
      let val = 0;
      if(ind < str2.length) {
        val += parseInt(str2[ind]);
      }
      val += parseInt(str1[ind]);
      if(val > 9) {
        arr_result.unshift(val - 10);
        if(ind + 1 < str1.length) {
          str1[ind+1] = (parseInt(str1[ind+1]) + 1).toString();
        }
        else {
          arr_result.unshift(1);
        }
      }
      else {
        arr_result.unshift(val);
      }
      ind++;
    }
  }
  else {
    while(ind < str2.length) {
      let val = 0;
      if(ind < str1.length) {
        val += parseInt(str1[ind]);
      }
      val += parseInt(str2[ind]);
      if(val > 9) {
        arr_result.unshift(val - 10);
        if(ind + 1 < str2.length) {
          str2[ind+1] = (parseInt(str2[ind+1]) + 1).toString();
        }
        else {
          arr_result.unshift(1);
        }
      }
      else {
        arr_result.unshift(val);
      }
      ind++;
    }
  }
  return arr_result.join("");
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts_num = 0;
  let comments_num = 0;
  for(let post of listOfPosts) {
    if(post.author === authorName) {
      posts_num++;
    }
    if(post["comments"]) {
      for(let comment of post.comments) {
        if(comment.author === authorName) {
          comments_num++;
        }
    }
    }
  }
  return `Post:${posts_num},comments:${comments_num}`;
};

const tickets=(people)=> {
  let clerk = {
    25 : 0,
    50 : 0,
    100 : 0
  };
  for(let person_money of people) {
    if(parseInt(person_money) === 25) {
      clerk['25']++;
    } 
    else if (parseInt(person_money) === 50) {
      if(clerk['25'] > 0) {
        clerk['25']--;
        clerk['50']++;
      }
      else {
        return 'NO';
      }
    }
    else {
      if(clerk['50'] > 0 && clerk['25'] > 0) {
        clerk['50']--;
        clerk['25']--;
        clerk['100']++;
      }
      else if(clerk['25'] >= 3) {
        clerk['25'] -= 3;
        clerk['100']++;
      }
      else {
        return 'NO';
      }
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
